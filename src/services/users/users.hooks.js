const { authenticate } = require('feathers-authentication').hooks;
const commonHooks = require('feathers-hooks-common');
const { restrictToOwner } = require('feathers-authentication-hooks');

const { hashPassword } = require('feathers-authentication-local').hooks;
const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: '_id',
    ownerField: '_id'
  })
];

const log = (data) => {
	return context => {
		console.log('<><><><><><><><><><><><>');
		console.log('data:',data);
		console.log('<><><><><><><><><><><><>');
		console.log('context:',context.data.facebook.profile.emails)
		console.log('<><><><><><><><><><><><>');
		
	}	
}

function customizeGoogleProfile(){
	return function(hook){
		if (hook.data.google) {
			hook.data.email = hook.data.google.profile.emails
				.find(email => email.type==='account').value
		}
		else if(hook.data.facebook){
			hook.data.email = hook.data.facebook.profile.emails[0].value
		}
		else{
			return Promise.reject();
		}
		
		return Promise.resolve(hook);
	}
}

function customizeFacebookProfile(){
	return function(hook){
		if(hook.data.facebook){
		
		}
	}
}

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ ...restrict ],
    create: [log(), customizeGoogleProfile(), hashPassword() ],
    update: [ ...restrict, hashPassword() ],
    patch: [ ...restrict, hashPassword() ],
//    remove: [ ...restrict ]
		remove:[]
  },

  after: {
    all: [
      commonHooks.when(
        hook => hook.params.provider,
        commonHooks.discard('password')
      )
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
