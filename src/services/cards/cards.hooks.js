const Ajv = require('ajv');
const commonHooks = require('feathers-hooks-common');
const authHooks = require('feathers-authentication-hooks');
const setupApproval = require('../../hooks/setup-approval.js');
const assignUser = require('../../hooks/assignUser.js');
const {
  validateSchema,
  setUpdatedAt,
  setCreatedAt,
} = commonHooks;

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authHooks.restrictToAuthenticated(),
      authHooks.associateCurrentUser(),
      assignUser(),
      validateSchema(cardSchema(), Ajv),
      setupApproval(),
      setCreatedAt(),
      setUpdatedAt(),
    ],
    update: [
      authHooks.restrictToOwner(),
      authHooks.restrictToOwner({ idField: 'email', ownerField: 'userEmail' }),
      setUpdatedAt(),
    ],
    patch: [
      commonHooks.debug(),
      authHooks.restrictToOwner({ idField: 'email', ownerField: 'userEmail' }),
      setUpdatedAt(),
    ],
    remove: [
      authHooks.restrictToOwner({ idField: 'email', ownerField: 'userEmail' }),
    ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


function cardSchema() {
  return {
    title: 'Card Schema',
    type: 'object',
    required: ['cardType', 'text', 'userId', 'userEmail'],
    properties: {
      cardType: {
        description: "the type of card. prompt or answer",
        enum: ["prompt", "answer"]
      },
      text: { type: 'string' },
      userEmail: { type: 'string' },
    }
  }
}
