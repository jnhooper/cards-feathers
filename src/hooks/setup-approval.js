// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// defaults approved to false
module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function setupApproval (hook) {
    // Hooks can either return nothing or a promise
    // that resolves with the `hook` object for asynchronous operations
    return new Promise((resolve, reject) => {
			hook.data.approved = false;
			resolve(hook)
		})
  };
};
