
module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function assignUser (hook) {
    // Hooks can either return nothing or a promise
    // that resolves with the `hook` object for asynchronous operations
    return new Promise((resolve, reject) => {
			console.log(hook.params.user.email);
			hook.data.userEmail = hook.params.user.email;
			resolve(hook)
		})
  };
};
