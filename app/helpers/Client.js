import feathers from 'feathers/client';
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client';
import io from 'socket.io-client';
import auth from 'feathers-authentication-client';

const socket = io();

const client = feathers();

client.configure(hooks());
client.configure(socketio(socket));
client.configure(auth({storage: window.localStorage, cookie: 'feathers-jwt'}))

const cards = client.service('cards');
const users = client.service('users');

export {
	cards,
	users,
	client,
}
