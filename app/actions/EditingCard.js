import { cards } from '../helpers/Client';
import { updateCard } from 'actions/Cards';

const SET_EDITING_TYPE = 'SET_EDITING_TYPE';
const SET_EDITING_CARD = 'SET_EDITING_CARD';
const STOP_EDITING_CARD = 'STOP_EDITING_CARD'; 

const setEditingCardToPrompt = () => ({
	type: SET_EDITING_TYPE,
	cardType: 'prompt',
});

const setEditingCardToAnswer = () => ({
	type: SET_EDITING_TYPE,
	cardType: 'answer',
});

const setEditingType = ( cardType ) => ({
	type: SET_EDITING_TYPE,
 	cardType,	
});

const setEditingCard = card => ({
	type: SET_EDITING_CARD,
	card,
});

const stopEditingCard = () => ({
	type: STOP_EDITING_CARD,
});

const patchCard = (text, cardType, id) => {
	return function (dispatch){
		return cards.patch(id, {text, cardType})
			.then(
				resp => {
					dispatch(stopEditingCard());
					dispatch(updateCard(resp));
				}
			);
	}
}

export {
	SET_EDITING_TYPE,
	SET_EDITING_CARD,
	STOP_EDITING_CARD,

	setEditingType,
	setEditingCardToPrompt,
	setEditingCardToAnswer,
	setEditingCard,
	stopEditingCard,
	patchCard,
}

