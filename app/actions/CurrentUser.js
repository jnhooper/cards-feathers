import { client, users } from 'helpers/Client';
import auth from 'feathers-authentication-client';
const REQUEST_OAUTH = 'REQUEST_OAUTH';
const USER_AUTHENTICATED = 'USER_AUTHENTICATED';
const JWT_PAYLOAD_RECEIVED = 'JWT_PAYLOAD_RECIEVED';
const SET_USER = 'SET_USER';
const LOG_OUT = 'LOG_OUT';

const setCurrentUser = user => ({
	type: SET_USER,
	user: user,
})

const authenticate =() =>{
	return function(dispatch) {
		client.authenticate()
			.then(response => {
				console.log('Authenticated!', response);
				return client.passport.verifyJWT(response.accessToken);
			})
			.then(payload => {
				console.log('JWT Payload', payload);
				return users.get(payload.userId);
			})
			.then(user => {
				console.log(user);
				dispatch(setCurrentUser(user));
				client.set('user', user);
			})
			.catch(function(error){
				console.error('Error Authenticating!', error);
			})
	}
}

const logOutUser = () => ({
	type: LOG_OUT,
});

const logOut = () => {
	return function(dispatch) {
		client.logout();
		dispatch(logOutUser());
		console.log(LOG_OUT);
	}
}
export {
	REQUEST_OAUTH,
	USER_AUTHENTICATED,
	JWT_PAYLOAD_RECEIVED,
	SET_USER,
	LOG_OUT,

	authenticate,
	logOut,
} 
