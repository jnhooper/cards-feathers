const SET_CARD_TYPE = 'SET_CARD_TYPE';

const setCardToPrompt = () => ({
	type: SET_CARD_TYPE,
	cardType: 'prompt',
});

const setCardToAnswer = () => ({
	type: SET_CARD_TYPE,
	cardType: 'answer',
});

export {
	SET_CARD_TYPE,

	setCardToPrompt,
	setCardToAnswer,
}
