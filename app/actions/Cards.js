import { cards } from '../helpers/Client';

const CREATE_CARD = 'CREATE_CARD';
const REQUEST_CARDS = 'REQUEST_CARDS';
const HYDRATE_CARDS = 'HYDRATE_CARDS';
const POST_CARD = 'POST_CARD';
const INSERT_CARD = 'INSERT_CARD';
const UPDATE_CARD = 'UPDATE_CARD';
const REMOVE_CARD = 'REMOVE_CARD';

const createCard = (text, cardType) => {
	return {
		text,
		cardType,
		type: CREATE_CARD,
	};
};

const requestCards = () => ({
	type: REQUEST_CARDS,
})

const postCard = (card) => ({
	type: POST_CARD,
	card,
});

const insertCard = (card) =>({
	type: INSERT_CARD,
	card,
});

const updateCard = (card) => ({
	type: UPDATE_CARD, 
	card,
});

const hydrateCards = (cards) => ({
	type: HYDRATE_CARDS,
	cards,
});

const getCards = () => {
	return function (dispatch) {
		dispatch(requestCards());
		
		return cards.find().then(
			resp => {
				dispatch(hydrateCards(resp.data));
			},
			error => console.error('error in request:', error)
			)
	} 
}

const addCard = (text, cardType) => {
	return function (dispatch) {
		dispatch(postCard({text, cardType}))

		return cards.create({
			text,
			cardType
		}).then(resp => {
				dispatch(insertCard(resp));
			},
			error => console.error('error in post', error)
		)
	}
}

const removeCard = (card) => ({
	type: REMOVE_CARD,
	card,
});

const deleteCard = (id) => {
	return function(dispatch) {
		if(id){
			return cards.remove(id)
				.then(resp => dispatch(removeCard(resp)));
		}
		else return (a) => a
	}
}


export {
	CREATE_CARD,
	REQUEST_CARDS,
	HYDRATE_CARDS,
	POST_CARD,
	INSERT_CARD,
	UPDATE_CARD,
	REMOVE_CARD,
	
	createCard,
	getCards,
	hydrateCards,
	addCard,
	updateCard,
	deleteCard,
};
