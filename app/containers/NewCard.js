import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addCard } from 'actions/Cards';
import {
	patchCard,
	setEditingCardToPrompt,
	setEditingCardToAnswer,
} from 'actions/EditingCard';
import {
	setCardToPrompt,
	setCardToAnswer,
} from 'actions/NewCard';

import Card from 'components/Card';
import CircleButton from 'components/CircleButton';

import css from './NewCard.scss';

class EditableCard extends React.Component {
	constructor(props) {
		super(props);
		this.sendCard = props.sendCard;
		this.defaultText = props.defaultText;
		this.switchToAnswer = props.switchToAnswer;
		this.switchToPrompt = props.switchToPrompt;
		this.handleClick = this.handleClick.bind(this);
		this.props = props;
	}

	handleClick() {
		this.sendCard(this.refs.textArea.innerHTML, this.props.cardType, this.props.cardId);	
		this.refs.textArea.innerHTML = this.defaultText;
	}

	render() {
		const textArea = this.props.newCard ? 
		 (
				<h1
					ref="textArea"
					contentEditable="true"
				>
				{this.defaultText}
				</h1>
			) : 
			( 
				<h3
					ref="textArea"
					contentEditable="true"
				>
					{this.defaultText}
				</h3>
		 	);
		const submit = this.props.newCard ? 
			(<h1>SUBMIT CARD</h1>) :
			(<h4>Save</h4>);
		return(
			<div className={css.card_container}>
					<div className={css.new_card}>
						<div className={css.card_body}>
							<div className={css.row}>
								<div className={`${css.color_button} ${css.left}`}>
									{
										this.props.color === 'black' &&
											(
												<CircleButton
													onClick={this.switchToAnswer}
													style={{backgroundColor: 'white', color:'black'}}
												>
													White
												</CircleButton>
											) ||
												<div className={css.placeholder}></div>
									}
								</div>
								<Card
									color={this.props.color}
									size={this.props.newCard ? 'large' : 'medium' }
								>
									{ textArea }
								</Card>
								<div className={`${css.color_button} ${css.right}`}>
									{
										this.props.color === 'white' &&
											(
												<CircleButton
													onClick={this.switchToPrompt}
													style={{backgroundColor: 'black', color:'white'}}
												>
													Black
												</CircleButton>
											) ||
												<div className={css.placeholder}></div>
									}
								</div>
							</div>
						</div>
						{
							this.props.currentUser.get('displayName') &&
								<div
									className={`${css.submit_card} ${this.props.newCard ? css.large : css.medium}`}
									onClick={this.handleClick}
								>
								{submit}
							</div>
						}
				</div>
			</div>
		)
	}
}

const mapDispatchToProps = (dispatch, props) => {
	const { newCard } = props;
	const toPrompt = newCard ? setCardToPrompt : setEditingCardToPrompt;
	const toAnswer = newCard ? setCardToAnswer : setEditingCardToAnswer;
	console.log(setEditingCardToPrompt, setEditingCardToAnswer)
	const sendCard = newCard ? bindActionCreators(addCard, dispatch) : 
		bindActionCreators(patchCard, dispatch);
	const switchToPrompt = bindActionCreators(toPrompt, dispatch);
	const switchToAnswer = bindActionCreators(toAnswer, dispatch);
	return {
		sendCard,
		switchToAnswer,
		switchToPrompt,
	}
}
const mapStateToProps = (state, props) => {
	const { newCard } = props;
	const cardState = newCard ? 'newCard' :'editingCard';
	const cardType = state.getIn([cardState,'cardType']);
	const currentUser = state.get('currentUser');

	return {
		cardType,
		currentUser,
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(EditableCard);
