import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { logOut } from 'actions/CurrentUser';
import {
	patchCard,
	setEditingCard,
} from 'actions/EditingCard';
import { deleteCard } from 'actions/Cards';
import Card from 'components/Card';
import CardList from 'components/CardList';
import CircleButton from 'components/CircleButton';
import EditableCard from 'containers/NewCard';
import UserContainer from 'containers/UserContainer';

import css from './App.scss';
import { client } from 'helpers/Client.js';

const App = ({
	cards,
	newCard,
	logout,
	currentUser,
	patch,
	setEditCard,
	editingCard,
	deleteCard,
	}) => {	
	console.log(cards);
	const cardList = cards.map(card =>{
		const cardItem = card.get('_id') === editingCard.get('_id') ? 
			(
			 <EditableCard
			 	newCard={ false }
				cardId={card.get('_id')}
				defaultText={ card.get('text') }
				color={ editingCard.get('cardType') === 'prompt' ? 'black' : 'white' }
			 />
			 ) : 
			(
				<div key={card.get('_id')}>
					<Card
						key={card.get('_id')}
						size='small'
						color={card.get('cardType')==='answer'? 'white' : 'black'}
					>
						<h4>{card.get('text')}</h4>
					</Card>
					{ 
						card.get('userEmail') === currentUser.get('email') &&
							<div>
								<a href="#" onClick={(e) =>{
									e.preventDefault();
									setEditCard(card);	
									}}
								>Edit </a>
								<a href="#" onClick={e => {
									e.preventDefault();
									deleteCard(card.get('_id'));
								}}>
								Remove
								</a>
							</div>
					}
				</div>
			)
		return cardItem
	}
	);
	const title = "John & Olga's";

			/*<a href="#" onClick={(e)=>{
				client.service('cards').remove()
			}}>
				NUKE EM
			</a>
			*/
	return (
		<div className={css.container}>
			<div className={css.header}>
				<div></div>
				<div>
					<h1>{title}</h1>
					<h2>Cards for Matrimonty</h2>
					<p>
						Cards for Matrimony is a fill in teh blank, answer teh prompt game. 
						Black cards represent prompts and white cards represent answers. Think 
						of it as Apples to Apples or Cards Against Humanity but for our family and 
						friends. (And built by our family and friends!) We will print these cards 
						out and have them available for playing at the cocktail hour.
					</p>
					<p>(Updates coming soon)</p>
				</div>
				<UserContainer />
			</div>
			<EditableCard
				newCard={true}
				color={newCard.get('cardType')=== 'prompt'? 'black' : 'white'}
				defaultText='Enter Text Here!'
			/>

			<div>
				<CardList>
					{cardList}
				</CardList>
			</div>
		</div>
	)
};

const mapStateToProps = ( state => {
	return{
		cards: state.get('cards'),
		newCard: state.get('newCard'),
		currentUser: state.get('currentUser'),
		editingCard: state.get('editingCard'),
	}
})

const mapDispatchToProps = (dispatch) => {
	const logout = bindActionCreators(logOut, dispatch);
	const patch = bindActionCreators(patchCard, dispatch);
	const setEditCard = bindActionCreators(setEditingCard, dispatch);
	
	return {
		logout,
		patch,
		setEditCard,
		deleteCard: bindActionCreators(deleteCard, dispatch),
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
