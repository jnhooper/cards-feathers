import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import UserIcon from 'components/UserIcon';

import { logOut } from 'actions/CurrentUser';

import css from './UserContainer.scss';
const UserContainer = ({
		user,
		logout,
	}) => {
	const icon = user.get('icon');
	const loggedIn = !!user.get('displayName')
	return (
		<div className={css.user_container}>
			<div className={css.user_content_container}>
			{
				loggedIn && icon &&
					<UserIcon src={icon} />
			}
			{
				loggedIn &&		
					<button href="#" onClick={(e)=> {
						e.preventDefault();
						logout();
					}}>
						Log out
					</button>
			}
			{
				!loggedIn &&
					<div>
						<a href="/auth/google" className="button"><h3>Login With Google</h3></a>
						<a href="/auth/facebook" className="button"><h3>Login With Facebook (not working yet)</h3></a>
					</div>
			}
			</div>
		</div>
	);	
};

const mapStateToProps = ( state => {
	return{
		user: state.get('currentUser'),
	}
})

const mapDispatchToProps = (dispatch) => {
	const logout = bindActionCreators(logOut, dispatch)
	return {
		logout,
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);
