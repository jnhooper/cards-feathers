import React from 'react';
import css from './UserIcon.scss';
const UserIcon = ({src}) => {
	return <img src={src} className={css.user_icon}/>
};

export default UserIcon;
