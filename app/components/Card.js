import React from 'react';

import css from './Card.scss';
import ds from '../scss/DropShadow.scss';

class Card extends React.PureComponent {
// const Card = ({children, footer, color, size}) => {
	render(){
		const {children, footer, color, size} = this.props;
		return (
			<div className={`${css.card} ${css[color]} ${css[size]} ${ds.drop_shadow}`}>
				<div className= { css.content }>
					{children}
				</div>
				<div className={ css.footer }>
					{ footer }
				</div>
			</div>
		)
	}
}

export default Card;
