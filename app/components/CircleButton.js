import React from 'react';

import css from './CircleButton.scss';
import ds from '../scss/DropShadow.scss';

const CircleButton = (props) => {
	return (
		<div
			style={props.style}
			className={`${css.circle_button} ${ds.drop_shadow}`}
			onClick={()=>props.onClick()}
		>
			{props.children}
		</div>		
	);
}

export default CircleButton;
