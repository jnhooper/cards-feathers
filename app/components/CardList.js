import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import css from './CardList.scss';

const CardList = ({
	children,
}) => {
	return (
		<div className={css.card_list_container}>
			{children}
		</div>
	);
};

export default CardList;
