import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Map } from 'immutable';
import { render } from 'react-dom';
import io from 'socket.io-client';
//import rest from 'feathers-rest';
import feathers from 'feathers/client';
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client';
import App from './containers/App';
import CardApp from 'reducers';
import { getCards } from 'actions/Cards';
import { authenticate, logOut } from 'actions/CurrentUser';
import localStorage from 'localstorage-memory';
// import auth from'@feathersjs/authentication-client';
import auth from 'feathers-authentication-client';

 const socket = io();
const client = feathers();

// Create the Feathers application with a `socketio` connection

/*client.configure(socketio(socket))

//client.configure(rest());
client.configure(hooks())
	.configure(auth({storage: window.localStorage, cookie: 'feathers-jwt'}))
	//.configure(feathers.authentication({cookie: 'feathers-jwt'}))

//client.logout().then(data=>console.log('logged out', data));
	
client.authenticate({
//	strategy: 'google',
})
	.then(response => {
		console.log('Authenticated!', response);
		return client.passport.verifyJWT(response.accessToken);
	})
	.then(payload => {
			console.log('JWT Payload', payload);
			console.log(window.localStorage)
			console.log("user_id",payload.userId)
				return client.service('users').get(payload.userId);
	})
	.then(user => {
		console.log('passed in user:', user)
			client.set('user', user);
				console.log('User', client.get('user'));
				client.service('users').find().then(data=> console.log('users:',data));
	})
	.catch(function(error){
			console.error('Error authenticating!', error);
	});
*/
/*const cards = client.service('cards')
const users = client.service('users');
users.find().then(data=>console.log('data',data));
*/
//users.remove();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
		CardApp,
		new Map({}),
		composeEnhancers(applyMiddleware(thunkMiddleware))
);

//cards.remove(null);
store.dispatch(getCards());
store.dispatch(authenticate());

render(
	<Provider store={store}>
		<div>
		<App client={client} />
		</div>
	</Provider>,
  document.getElementById('app')
);

module.hot.accept();
