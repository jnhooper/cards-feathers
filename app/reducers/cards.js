import { fromJS } from 'immutable';
import {
	CREATE_CARD,
	INSERT_CARD,
	HYDRATE_CARDS,
	UPDATE_CARD,
	REMOVE_CARD
} from '../actions/Cards.js'; 

const cards = (state = fromJS([]), action) => {
	switch (action.type) {
		case HYDRATE_CARDS: 
			return fromJS(action.cards).sort((a, b) => {
				if(a.get('updatedAt') === undefined){ return 1 }
				if(b.get('updatedAt') === undefined){ return -1 }
				else{
					return new Date(a.get('updatedAt')) < new Date(b.get('updatedAt')) ? 1 : -1;
				}
			});
		
		case INSERT_CARD:
			return state.insert(0, fromJS(action.card));
		
		case UPDATE_CARD:
			return state.map(card => {
				if(card.get('_id') === action.card._id){
					return fromJS(action.card);
				}
				else{
					return card;
				}
			})
		
		case REMOVE_CARD:
			return state.filter(card => card.get('_id')!== action.card._id);
		
		default:
			return state;
	}
}

export default cards;

