import { combineReducers } from 'redux-immutable';
import Immutable from 'immutable';
import cards from 'reducers/cards';
import newCard from 'reducers/newCard';
import users from 'reducers/users';
import currentUser from 'reducers/currentUser';
import editingCard from 'reducers/editingCard';

const CardApp = combineReducers({
	cards,
	newCard,
	users,
	currentUser,
	editingCard,
})

export default CardApp
