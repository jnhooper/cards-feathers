import { fromJS } from 'immutable';
import {
	SET_EDITING_TYPE,
	SET_EDITING_CARD,
	STOP_EDITING_CARD,
} from 'actions/EditingCard';

const editingCard = (state = fromJS({}), action) => {
	switch (action.type) {
		case SET_EDITING_TYPE:
			return state.set('cardType', action.cardType);
		case SET_EDITING_CARD:
			return fromJS(action.card);
		case STOP_EDITING_CARD:
			return fromJS({});
		default:
			return state;
	}
}

export default editingCard;
