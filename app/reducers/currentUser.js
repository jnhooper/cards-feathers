import { fromJS } from 'immutable';
import { SET_USER, LOG_OUT } from 'actions/CurrentUser';

const currentUser = (state = fromJS({}), action) => {
	switch (action.type) {
		case SET_USER:
			const newState = Object.assign({}, action.user);
			if(action.user.google){
				newState.displayName = newState.google.profile.displayName;	
				newState.icon = newState.google.profile.photos[0].value;
			}
			if(action.user.facebook){
				newState.displayName = newState.facebook.profile.displayName;
				newState.icon = newState.facebook.profile.photos[0].value;
			}
			return fromJS(newState);
		case LOG_OUT:
			return fromJS({});
		default:
			return state;
	}
}

export default currentUser;

