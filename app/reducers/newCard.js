import { fromJS } from 'immutable';
import {
	SET_CARD_TYPE,
} from '../actions/NewCard.js';

const newCard = (state = fromJS({cardType: 'answer'}), action) => {
	switch (action.type) {
		case SET_CARD_TYPE:
			return state.set('cardType', action.cardType);
		default:
			return state;
	}
}

export default newCard;
