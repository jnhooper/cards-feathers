const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client?http://localhost:3030/',
    './app/index.js',
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle.js',
  },
	devtool: 'inline-source-map',
	resolve:{
		alias: {
			reducers: path.resolve(__dirname, 'app/reducers'),
			actions: path.resolve(__dirname, 'app/actions'),
			helpers: path.resolve(__dirname, 'app/helpers'),
			components: path.resolve(__dirname, 'app/components'),
			containers: path.resolve(__dirname, 'app/containers'),
		},
	},
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_module/,
        use: [
          { loader: 'react-hot-loader/webpack' },
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'react', 'stage-2'],
              plugins: ['react-hot-loader/babel'],
            },
          },
        ],
      },
			{
				test: /\.scss$/,
				use: [
					{ 
						loader: "style-loader",
						options:{
							modules: true,
							localIdentName: '[path][name]__[local]--[hash:base64:5]'
						}
				 	},
					{ loader: "css-loader",
						options:{
							modules: true,
							localIdentName: '[path][name]__[local]--[hash:base64:5]'
						}
					},
					{ loader: "sass-loader",
						options:{
							modules: true,
							localIdentName: '[path][name]__[local]--[hash:base64:5]'
						}
					},
				]
			}
    ],
  },
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, 'app')],
    extensions: ['.js', '.jsx'],
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
};
